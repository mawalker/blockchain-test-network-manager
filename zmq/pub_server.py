#!/usr/bin/python3

import zmq
import random
import sys
import time
import pycurl
import json
from io import BytesIO

def rpcCommand(method,params=[],ip='localhost',port='9012',id=1,jsonrpc="2.0",verbose=False,exceptions=False):
    """ Method to abstract away 'curl' usage to interact with RPC of geth clients. """
    # the <ip:port> to connect to
    ipPort = str(ip) + ":" + str(port)
    # buffer to capture output
    buffer = BytesIO()
    # start building curl command to process   
    try:
        c = pycurl.Curl()
        c.setopt(pycurl.URL, ipPort)
        c.setopt(pycurl.HTTPHEADER, ['Accept:application/json'])
        c.setopt(pycurl.WRITEFUNCTION, buffer.write)
        data2 = {"jsonrpc":str(jsonrpc),"method": str(method),"params":params,"id":str(id)}
        data = json.dumps(data2)
        c.setopt(pycurl.POST, 1)
        c.setopt(pycurl.POSTFIELDS, data)
        if verbose:
            c.setopt(pycurl.VERBOSE, 1)
        #perform pycurl

        c.perform()

        # check response code (HTTP codes)
        if (c.getinfo(pycurl.RESPONSE_CODE) != 200):
            if exceptions:
                raise Exception('rpc_communication_error', 'return_code_not_200')
            return {'error':'rpc_comm_error','desc':'return_code_not_200','error_num':None}
        #close pycurl object
        c.close()
    except pycurl.error as e:
        c.close()
        errno, message = e.args
        if exceptions:
            raise Exception('rpc_communication_error', 'Error No: ' + errno + ", message: " + message)
        return {'error':'rpc_comm_error','desc':message,'error_num':errno}


    # decode result
    results = str(buffer.getvalue().decode('iso-8859-1'))
    if verbose:
        print (results)

    # convert result to json object for parsing
    data = json.loads(results)
    # return appropriate result
    if 'result' in list(data.keys()):
        return data["result"]
    else:
        if 'error' in list(data.keys()):
            if exceptions:
                raise Exception('rpc_communication_error', data)
            return data
        else:
            if exceptions:
                raise Exception('rpc_communication_error', "Unknown Error: possible method/parameter(s) were wrong and/or networking issue.")
            return {"error":"Unknown Error: possible method/parameter(s) were wrong and/or networking issue."}

##############################################################################
# Simple helper method to simplify getting peer count of a client.
##############################################################################   

def getPeerCount(ip,port,verbose=False):  
    results = rpcCommand(ip=ip,port=port,verbose=False,method="net_peerCount",params=[])
    if (verbose != False):
        print (results)
    return results


################

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

oldPeerCount = ""
while True:
    newPeerCount = getPeerCount("10.4.209.25","9006",verbose=True)
    time.sleep(5)
    #print ("new:" + newPeerCount + " old:" + oldPeerCount)
    if (newPeerCount != oldPeerCount):
        print(("new peer count:" + newPeerCount))
        oldPeerCount = newPeerCount
        # Topic must be a number
        topic = 100
        messagedata = int(newPeerCount,16)
        # print "%d %d" % (topic, messagedata)
        # had to switch to send_string & recv_string(...) in sub due to moving to python3 & utf8 support
        socket.send_string("%d %d" % (topic, messagedata))
        # can also send python objects.
        # socket.send_pyobj({1:[1,2,messagedata]})
    time.sleep(1)
