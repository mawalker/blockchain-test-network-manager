from fabric.api import env, run, execute, task
from fabric.operations import run, put, local
from fabric.contrib.project import rsync_project
from fabric.api import settings, hide

from io import StringIO
from random import *
import sys
import warnings
# ignore that an SSH dep. of Fabric will need Fabric to be upgraded in the future.
warnings.simplefilter(action='ignore', category=FutureWarning)
env.use_ssh_config = True  


class FabricException(Exception):
    # error handling class for exceptions when processes return error (such as kill -9 <pid>)
    pass



def runbg(cmd, processname='', stdout=None ,sockname="dtach"):
    """ run any process in the background, uses 'dtach' to allow processing even after Fab disconnects.
    """
    # mktemp uses name of process and XX* pattern to determin name of temp file, which means:
    # same pattern + same process name = same name, which is obviously bad in this case,
    # so we insert a random number ourselves beforehand for each new process.
    randValue = '`mktemp -u /tmp/' + str(int(randint(1,9999999))).rjust(7,'0')
    if (processname != ''):
        cmd = 'bash -c \'exec -a ' + str(processname) + ' '  + str(cmd) + '\''
    if stdout is None:
        return run('dtach -n ' + randValue + '`.%sXXXXX.dtach` %s'  % (sockname,cmd))
    else:
        return run('dtach -n ' + randValue + '`.%sXXXXX.dtach` %s'  % (sockname,cmd),stdout)



def evokebg(cmd, processname='',hosts=[], stdout=None ,sockname="dtach"):
    """ run any process in the background, uses 'dtach' to allow processing even after Fab disconnects.
    """
    # mktemp uses name of process and XX* pattern to determin name of temp file, which means:
    # same pattern + same process name = same name, which is obviously bad in this case,
    # so we insert a random number ourselves beforehand for each new process.
    randValue = '`mktemp -u /tmp/' + str(int(randint(1,9999999))).rjust(7,'0')
    if (processname != ''):
        cmd = 'bash -c \'exec -a ' + str(processname) + ' '  + str(cmd) + '\''
    return execute('dtach -n ' + randValue + '`.%sXXXXX.dtach` %s'  % (sockname,cmd), hosts)

