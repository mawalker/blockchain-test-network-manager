#!/usr/bin/python3

##############################################################################
#
# CLI tool to manage Ethereum Test Network.
#
#	Manages:
#		Clients
#		Blockchain
#		Miners
#		bootnodes
#
# @Author Michael A. Walker
# @Date 2017-06-27
#
##############################################################################

##############################################################################
# imports
##############################################################################
from io import StringIO
from subprocess import check_output,call
import sys, os
import json
import os.path
import pprint
import datetime
from fabric.api import run, execute, task
from io import StringIO
from random import *
from fabric.api import env

env.use_ssh_config = True

##############################################################################
#
##############################################################################

def to_camel_case(name):
# convert a string to camel_case all lower case.
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def readJsonFile(jsonFile, verbose=False):
    if verbose:
        print("Reading Json file, verbose is true, jsonFile=" + jsonFile)
# read in a json file and print it if verbose
    if os.path.isfile(jsonFile) != True:
        print("Operand provided to be the network config file was not a file.")
        sys.exit(2)
        return

    with open(str(jsonFile)) as data_file:
        data = json.load(data_file)

    # read in the configuration JSON file
    if verbose:
        print("\nRead in the following network-config json file:\n")
        print(json.dumps(data, indent=4, sort_keys=False) + "\n")

    return data



def readConfigFile(jsonFile='./test-config.json',
                            verbose=False
                           ):
    if verbose:
        print("\n readConfigFile for Test Verbose, config file" + str(jsonFile) + "\n")
    data = readJsonFile(jsonFile, verbose)
    now = datetime.datetime.now()

    try:

        ##########################################################################
        # get the values from the JSON file
        ##########################################################################

        # network specific data
        configurationName = data["configurationName"]
        configurationVersion = data["configurationVersion"]
        clientTypes = data["clients"]
        startTime = now.strftime("%Y_%m_%d_%I_%M_%p")

    except KeyError as inst:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(("KeyError:  " + str(inst)  + " was invalid in file: " + str(fname) +  " at line: " + str(exc_tb.tb_lineno) ))
        print ("Please Try again...")
        sys.exit(3)

    return configurationName, configurationVersion, clientTypes, startTime


def startTest(configurationName , configurationVersion, clientTypes, startTime, verbose=False):
    verboseResults = ''
    data = clientTypes
    try:

        ##########################################################################
        # get the values from the JSON data
        ##########################################################################
        print (" StartTime: " + startTime)

        numClientTypes = len(clientTypes)
        pp = pprint.PrettyPrinter(indent=4)
#        pp.pprint(clientTypes)
        for clientType in clientTypes:
            print(" new clientType")
#            pp.pprint(clientType)

            data = clientTypes[clientType]
#            pp.pprint(data)

            #parameters they both share
            command = data["command"]
            verbosity = data["verbosity"]
            hosts = data["hosts"]
            numHosts = len(hosts)
            pubsub = data["pubsub"]
            noPrint = data["noPrint"]
            domain = data["domain"]
            numSubs = data["numSubscribers"]
            numPubs = data["numPublishers"]

            if "durability" in data:
                durability = data["durability"]
                command += " -durability " + str(durability)

            if "instances" in data and "keyed" in data and data["keyed"] == "true":
                instances = data["instances"]
                command += " -keyed -instances " + str(instances)

            timestamps = data["timestamps"]

            # set noPrint arg.
            if noPrint == 'true':
                command += " -noPrint"
            # set domain arg.
            command += " -domain " + str(domain)
            # set verbosity arg.
            command += " -verbosity " + str(verbosity)

            if pubsub == 'pub':
                if verbose:
                    print("is publisher")
                #publisher only parameters
                command += " -pub"

                latencyCount = data["latencyCount"]
                command += " -latencyCount " + str(latencyCount)

                execNum = data["exec"]
                command += " -exec " + execNum

                if "scan" in data:
                    scan = data["scan"]
                    command += " -scan " + scan
                else:
                    if "datalen" in data:
                        datalen = data["dataLen"]
                        command += " -dataLength " + str(datalen)
                command += " -numSubscribers " + str(numSubs)

            else:
                if verbose:
                    print("is subscriber")
                command += " -sub"
                command += " -numPublishers " + str(numPubs)



            command += " >> perftest.out 2>&1"
            if timestamps == 'true':
                if verbose:
                    print ("timestamps true")
                # add 'date' at begining and end of command if timestamped.
#                command = "date >> perftest.out && " + command + " >> perftest.out 2>&1 && date >> perftest.out"
            if verbose:
                print (command)
            verboseResults += "\ncommand: " + str(command)

#            finalCommand = "runbg"
#            finalCommand += ":" + "\"" + str(command) + "\""
#            finalCommand += "," + "\"preftest\""
#            print ("\n\n")
#            print (str(finalCommand))
#            print ("\n\n")

            processname = "perftest"
            randValue = '`mktemp -u /tmp/' + str(int(randint(1,9999999))).rjust(7,'0')
            cmd = 'bash -c \'exec -a ' + str(processname) + ' '  + str(command) + '\''
            values = 'dtach -n ' + randValue + '.%sXXXXX.dtach` %s'  % ("perftest",cmd)
            
         
            for hostb in hosts:
                print (str(hostb))
                rValue = execute(runbg,values,host=hostb)
                print (str(rValue))

#            results = evokebg(finalCommand, hosts)
#            print (str(results))

            # call 'fab' with the correct host and parameters.
#            for host in hosts:
 #               print ("host: "+str(host))
#                results = check_output(["fab", "-H", str(host), finalCommand ])
  #              verboseResults += "\nresults: " + str(results)
   #         if verbose:
    #            print ("Verbose Results: " + verboseResults)
    #        lines = results.split('\n')
#            runbg(command)

    except KeyError as inst:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(("KeyError:  " + str(inst)  + " was invalid in file: " + str(fname) +  " at line: " + str(exc_tb.tb_lineno) ))
        print ("Please Try again...")
        sys.exit(3)

@task
def runbg(cmd):
    return run(cmd)

def perftestRun(jsonFile,verbose=False):

    results = ''

    if verbose:
        print("** perftestRun Start **")

    if os.path.isfile(jsonFile) != True:
        print("Operand provided to be the genesis block config file was not a file.")
        sys.exit(2)
        return

    # get test global variables
    configurationName, configurationVersion, clientTypes, startTime = readConfigFile(jsonFile,verbose)

    # run tests for each clientType
    startTest(configurationName,
              configurationVersion,
              clientTypes,
              startTime,
              verbose=verbose)




    if verbose:
        print("** Verbose Output: **")
        print(results)




def perftestStop(jsonFile,verbose=False):

    results = ''

    if verbose:
        print("** perftest perftestStop **")

    if os.path.isfile(jsonFile) != True:
        print("Operand provided to be the genesis block config file was not a file.")
        sys.exit(2)
        return




    if verbose:
        print("** Verbose Output: **")
        print(results)







def collectLogs(jsonFile,verbose=False):

    results = ''

    if verbose:
        print("** perftest collectLogs Start **")

    if os.path.isfile(jsonFile) != True:
        print("Operand provided to be the genesis block config file was not a file.")
        sys.exit(2)
        return




    if verbose:
        print("** Verbose Output: **")
        print(results)





def deleteLogs(jsonFile,verbose=False):

    results = ''

    if verbose:
        print("** perftest deleteLogs Start **")

    if os.path.isfile(jsonFile) != True:
        print("Operand provided to be the genesis block config file was not a file.")
        sys.exit(2)
        return




    if verbose:
        print("** Verbose Output: **")
        print(results)





