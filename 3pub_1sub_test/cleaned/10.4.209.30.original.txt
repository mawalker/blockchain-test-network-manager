
Setting verbosity to ERROR

Perftest Configuration:
	Reliability: Reliable
	Keyed: No
	Subscriber ID: 0
	Receive using: Listeners
	Domain: 27
	Dynamic Data: No
	XML File: perftest_qos_profiles.xml

Transport Configuration:
	Kind: UDPv4 & SHMEM (taken from QoS XML file)
	Use Multicast: False

Waiting to discover 3 publishers ...
Waiting for data...
RTI Perftest 2.4.0 (RTI Connext DDS 5.3.1)
Length:    32  Packets:   130560  Packets/s(ave):   60423  Mbps(ave):    15.5  Lost:     0 (0.00%) 
Length:    64  Packets:     3328  Packets/s(ave):  383410  Mbps(ave):   196.3  Lost: 3706072 (99.91%) 
Length:    32  Packets:     4096  Packets/s(ave):  481089  Mbps(ave):   123.2  Lost: 1848888 (99.78%) 
Length:    64  Packets:     2816  Packets/s(ave):  193539  Mbps(ave):    99.1  Lost: 3693528 (99.92%) 
Length:    32  Packets:     2560  Packets/s(ave):  365766  Mbps(ave):    93.6  Lost: 1843768 (99.86%) 
Length:    64  Packets:     5760  Packets/s(ave):  272276  Mbps(ave):   139.4  Lost: 9145680 (99.94%) 
Length:   128  Packets:     1472  Packets/s(ave):  424329  Mbps(ave):   434.5  Lost: 8957136 (99.98%) 
Length:    64  Packets:     3840  Packets/s(ave):  390839  Mbps(ave):   200.1  Lost: 7770952 (99.95%) 
Length:   128  Packets:      768  Packets/s(ave):  494208  Mbps(ave):   506.1  Lost: 8946000 (99.99%) 
Length:    64  Packets:     1408  Packets/s(ave):  198841  Mbps(ave):   101.8  Lost: 7744584 (99.98%) 
Length:   128  Packets:     5760  Packets/s(ave):  121835  Mbps(ave):   124.8  Lost: 8935504 (99.94%) 
Length:    64  Packets:     3200  Packets/s(ave):  224719  Mbps(ave):   115.1  Lost: 7741384 (99.96%) 
Length:   128  Packets:     1600  Packets/s(ave):  265957  Mbps(ave):   272.3  Lost: 1197184 (99.87%) 
Length:   256  Packets:     2336  Packets/s(ave):  140612  Mbps(ave):   288.0  Lost: 10072512 (99.98%) 
Length:   128  Packets:      704  Packets/s(ave):  181209  Mbps(ave):   185.6  Lost: 8772520 (99.99%) 
Length:   256  Packets:      288  Packets/s(ave):  111801  Mbps(ave):   229.0  Lost: 10068160 (100.00%) 
Length:   128  Packets:       64  Packets/s(ave):   67085  Mbps(ave):    68.7  Lost: 8763112 (100.00%) 
Length:   256  Packets:      448  Packets/s(ave):  280701  Mbps(ave):   574.9  Lost: 10066176 (100.00%) 
Length:   128  Packets:     1472  Packets/s(ave):  208794  Mbps(ave):   213.8  Lost: 8758696 (99.98%) 
Length:   256  Packets:      832  Packets/s(ave):   34689  Mbps(ave):    71.0  Lost: 10064224 (99.99%) 
Length:   128  Packets:     2304  Packets/s(ave):  196486  Mbps(ave):   201.2  Lost: 8756392 (99.97%) 
Length:   256  Packets:      896  Packets/s(ave):  347152  Mbps(ave):   711.0  Lost: 10057696 (99.99%) 
Length:   128  Packets:     1408  Packets/s(ave):   95173  Mbps(ave):    97.5  Lost: 8754408 (99.98%) 
Length:   256  Packets:     1088  Packets/s(ave):  111704  Mbps(ave):   228.8  Lost: 10055008 (99.99%) 
Length:   128  Packets:      448  Packets/s(ave):  432850  Mbps(ave):   443.2  Lost: 8735592 (99.99%) 
Length:   256  Packets:      128  Packets/s(ave):   69264  Mbps(ave):   141.9  Lost: 10040640 (100.00%) 
Length:   512  Packets:       80  Packets/s(ave):   22675  Mbps(ave):    92.9  Lost: 11709120 (100.00%) 
Length:  1024  Packets:      120  Packets/s(ave):    8298  Mbps(ave):    68.0  Lost: 12211096 (100.00%) 
Length:   512  Packets:       96  Packets/s(ave):   10920  Mbps(ave):    44.7  Lost: 9073928 (100.00%) 
Length:  1024  Packets:       96  Packets/s(ave):   59479  Mbps(ave):   487.3  Lost: 12208824 (100.00%) 
Length:   512  Packets:      352  Packets/s(ave):  188638  Mbps(ave):   772.7  Lost: 9066008 (100.00%) 
Length:  1024  Packets:       40  Packets/s(ave):   21917  Mbps(ave):   179.6  Lost: 12207464 (100.00%) 
Length:   512  Packets:       48  Packets/s(ave):  105726  Mbps(ave):   433.1  Lost: 9064888 (100.00%) 
Length:  1024  Packets:      664  Packets/s(ave):   61169  Mbps(ave):   501.1  Lost: 12206224 (99.99%) 
Length:   512  Packets:      736  Packets/s(ave):   80498  Mbps(ave):   329.7  Lost:     0 (0.00%) 
Length:  1024  Packets:      464  Packets/s(ave):   70420  Mbps(ave):   576.9  Lost: 9056536 (99.99%) 
Length:  2048  Packets:       52  Packets/s(ave):   18518  Mbps(ave):   303.4  Lost: 12024920 (100.00%) 
Length:  1024  Packets:       16  Packets/s(ave):   18518  Mbps(ave):   151.7  Lost: 8860664 (100.00%) 
Length:  2048  Packets:       64  Packets/s(ave):   23154  Mbps(ave):   379.4  Lost: 12024388 (100.00%) 
Length:  1024  Packets:      120  Packets/s(ave):   10525  Mbps(ave):    86.2  Lost: 8859976 (100.00%) 
Length:  2048  Packets:       20  Packets/s(ave):   29985  Mbps(ave):   491.3  Lost: 12024232 (100.00%) 
Length:  1024  Packets:        8  Packets/s(ave):   13029  Mbps(ave):   106.7  Lost: 8859296 (100.00%) 
Length:  2048  Packets:       44  Packets/s(ave):    8523  Mbps(ave):   139.7  Lost: 12023792 (100.00%) 
Length:  1024  Packets:      712  Packets/s(ave):   61215  Mbps(ave):   501.5  Lost: 8857664 (99.99%) 
Length:  2048  Packets:        4  Packets/s(ave):     531  Mbps(ave):     8.7  Lost: 12023380 (100.00%) 
Length:  1024  Packets:      376  Packets/s(ave):   42824  Mbps(ave):   350.8  Lost: 8856120 (100.00%) 
Length:  2048  Packets:       36  Packets/s(ave):   33994  Mbps(ave):   557.0  Lost: 12022652 (100.00%) 
Length:  4096  Packets:      114  Packets/s(ave):   20184  Mbps(ave):   661.4  Lost: 12106108 (100.00%) 
Length:  2048  Packets:      112  Packets/s(ave):   24205  Mbps(ave):   396.6  Lost: 8802416 (100.00%) 
Length:  4096  Packets:       36  Packets/s(ave):    3937  Mbps(ave):   129.0  Lost: 12105986 (100.00%) 
Length:  8192  Packets:      106  Packets/s(ave):    6857  Mbps(ave):   449.4  Lost:     0 (0.00%) 
Length: 16384  Packets:        7  Packets/s(ave):     624  Mbps(ave):    81.9  Lost: 11998049 (100.00%) 
Length:  8192  Packets:       90  Packets/s(ave):    7632  Mbps(ave):   500.2  Lost: 8618298 (100.00%) 
Length: 16384  Packets:        8  Packets/s(ave):    4744  Mbps(ave):   621.9  Lost: 11997844 (100.00%) 
Length:  8192  Packets:       14  Packets/s(ave):    9681  Mbps(ave):   634.5  Lost: 8617832 (100.00%) 
Length: 16384  Packets:        7  Packets/s(ave):    1810  Mbps(ave):   237.3  Lost: 11997647 (100.00%) 
Length: 32768  Packets:        9  Packets/s(ave):    1441  Mbps(ave):   377.9  Lost: 11970415 (100.00%) 
Length: 16384  Packets:        3  Packets/s(ave):    6410  Mbps(ave):   840.2  Lost: 8586812 (100.00%) 
Length: 32768  Packets:        4  Packets/s(ave):     255  Mbps(ave):    66.9  Lost: 11970320 (100.00%) 
