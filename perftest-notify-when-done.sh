#!/bin/bash
START=$(date)
echo "Started: "
echo $START

COUNT=$(./perftest-check-running-processes.sh | grep numSub | wc -l)

while [ $COUNT -gt 0 ]
do

sleep 10
COUNT=$(./perftest-check-running-processes.sh | grep numSub | wc -l)
COUNT2=$(expr $COUNT / 2)
echo "Sleeping 10 more seconds since number of active publishers was: $COUNT2"

done

echo "Started: "
echo $START
echo "Finished: "
date
