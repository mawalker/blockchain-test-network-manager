#!/bin/bash

##############################################################################
#
# Simple script to create a new test network based on provided config file.
#
# To use -> ./create <network-config-file.json>
#
# @Author Michael A. Walker
# @Date   2017-08-09
#
##############################################################################

### How to start a test network


# simple timestamp method
timestamp() {
  date +"%T"
}


# Start the clients (Dso(s) & Prosumer(s))
timestamp

echo "miners start"
time ./network-manager.py miners start --file $1

timestamp

echo "clients start"
time ./network-manager.py clients start --file $1


timestamp

OUTPUT=`./test/pycurlGetPeerCount.py 10.4.209.25 9010`



while [[ $OUTPUT -ne "0x0" ]]
do

sleep 1

OUTPUT=`./test/pycurlGetPeerCount.py 10.4.209.25 9010`

#echo $OUTPUT

done


timestamp
echo "done"

echo "sleeping 10 seconds to allow miners & clients to start up completely -for sure"
sleep 60
# Connect the network

timestamp

# Gets the enode of each client, & adds each client as a peer of each miner
# This creates a star-graph from each miner to each prosumer/dso.
echo "miners connect"
time ./network-manager.py miners connect --file $1

timestamp

#echo "network stop"
#time ./network-manager.py network stop --file $1

#timestamp

#echo "network delete"
#time ./network-manager.py network delete --file $1

#timestamp

## Network is now running and connected

