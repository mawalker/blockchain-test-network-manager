#!/bin/sh

# use fabfile in top of platibart repo to run this command on each host 1-6
fab -H eth1,eth2,eth3,eth4,eth5,eth6 runCommand:'cat perftest.out'
